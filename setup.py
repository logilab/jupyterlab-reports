import os
from setuptools import setup
from setuptools.command.build_py import build_py
import subprocess as subp


setup(name='jupyterlab_reports',
      version='0.1',
      description='Execute and convert notebooks to html',
      author='ogiorgis@logilab',
      author_email='contact@logilab.fr',
      license='GPLv2',
      python_requires='>=3.7',
      install_requires=[
          'jupyterlab',
      ],
      packages=['jupyterlab_reports',],
      zip_safe=False)
