# JupyterLab Reports

## Installation

First clone the repo
```bash
git clone https://gitlab.com/logilab/jupyterlab-reports
cd jupyterlab-reports
python setup.py install
jupyter serverextension enable --py jupyterlab_reports
```
