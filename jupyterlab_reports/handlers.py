import nbformat
from nbconvert import HTMLExporter
from nbconvert.preprocessors import ExecutePreprocessor
from notebook.notebookapp import NotebookWebApplication
from notebook.utils import url_path_join
from notebook.base.handlers import path_regex, IPythonHandler


def setup_handlers(web_app: NotebookWebApplication) -> None:
    host_pattern = ".*$"
    web_app.add_handlers(
        host_pattern,
        [
            (
                url_path_join(
                    web_app.settings["base_url"],
                    "/reports%s" % path_regex,
                ),
                FormatAPIHandler,
            )
        ],
    )


def to_html(nb_filename):
    ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
    with open(nb_filename) as f:
        nb_content = nbformat.read(f, as_version=4)
    ep.preprocess(nb_content)
    html_exporter = HTMLExporter()
    html_exporter.exclude_input = True
    html_exporter.exclude_input_prompt = True
    (body, _) = html_exporter.from_notebook_node(nb_content)
    return body


class FormatAPIHandler(IPythonHandler):

    def get(self, path=''):
        self.write(to_html(path[1:]))
